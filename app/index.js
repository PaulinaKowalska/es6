// const emails = ['paula@xp.pl', 'asia@ps.pl', 'asa@.od.pl'];
// emails.push('ss@ss.pl');
// console.log(emails);

// function hello() {
//     let message = "Hello!";
//     console.log(message);
// }
// function greeting() {
//     let message = "How are you?";
//     console.log(message);
// }
// hello();
// greeting();

//template string
// let a = `good`;
// let greeting = `${a} morning`;
// console.log(greeting);
// let b = 'birthday';
// let c = `Happy ${b}`;
// console.log(c);

//spread Operator and Rest Parameterss
// let a = [20, 30, 40];
// let b = [10, ...a, 50];
// console.log(b);
// let a = ['Dana', 'Erik', 'Frank'];
// let b = ['Alice', 'Bob', 'Carl', ...a];
// console.log(b);
// function collect(...a) {
//     console.log(a);
// }
// collect(1, 2, 3, 4, 5);

//Destructuring assignment on arrays
// let z = [4, 5, 6];
// let four = z[0]; // pierwszy element
// let five = z[1]; //wypisze 2 element
// console.log(four, five);
// let z = [4, 5, 6];
// let [four, five] = z;
// console.log(four, five);
// let animals = ["Simba", "Zazu", "Ed"];
// let [lion, bird] = animals;
// console.log(lion, bird);
//Destructuring assignment on obiects
// let king = {name: 'Mufasa', kids: 1};
// // let name = king.name;
// // let kids = king.kids;
// let { name, kids} = king;
// console.log(name, kids);
// let son = { name: 'Simba', parents: 2}
// let name, parents;
// ({name, parents} = son);
// console.log(name, parents);

//Arrow Functions =>
// function cheer() {
//     console.log("Wooho!");
// }
// cheer();
//
// var cheer = function () {
//     console.log("x");
// }
// cheer();
//
// setTimeout(function () {
//     console.log("Woohoo!");
// }, 3000);
// setTimeout(() => {
//     console.log("Woohoo!");
// }, 2000);
//
// let cheer = () => {
//     console.log("Wooho!");
// }
// cheer();

//Map Method
// let values = [20, 30, 40];
// // // let double = (n) => {
// // //     return n*2;
// // // }
// // // let doubled = values.map(double);  //values()-metoda pomocnicza do ustawienia iteracji
// // let doubled = values.map((n) => {
// //     return n*2;
// // })
// // console.log(doubled);
// //
// let doubled = values.map((n) => n*2);
// console.log(doubled);
//Filter Method
// let points = [7, 13, 45, 12, 2, 5, 6];
// let highScores = points.filter((n) => n>7);
// console.log(highScores);

//String helper method
// let b = "wwoh" + "oo".repeat(50);
// console.log(b);
// let b = `woo${"oo".repeat(50)}`;
// console.log(b);
//
// console.log("butterfly".startsWith("butter"));  //true
// console.log("butterfly".startsWith("fly")); //false
// console.log("butterfly".endsWith("fly"));
// console.log("butterfly".includes("fly")); // true-zawiera
// console.log("butterfly".includes("xx"));    //false

//helper method for numbers
// const addToCart = (item, number) => {
//     return Number.isFinite(number);
// }
// console.log(addToCart('shirt', Math.pow(2, 45)));
//
// const addToCart = (item, number) => {
//     return Number.isSafeInteger(number);
// }
// console.log(addToCart('shirt', Math.pow(2,54))); //false

//Modules
// import { fellowship, total } from './fellowship';
// console.log(fellowship, total);
// import multiply from './math';
// console.log(multiply(5 ,10));

//classes
// import Animal from './animal';
//
// class Lion extends Animal {
//     constructor(name, height, color) {
//         super(name, height);
//         this.color = color;
//     }
//     hello() {
//         console.log(`Hi! I'm ${this.name} from Pride Rock!`);
//     }
// }
// let son = new Lion("Simba", 2, "golden");
// // console.log(son);
// son.hello();
//
// //static methods
// class Calculator {
//     static multiply(a, b) {
//         return a*b;
//     }
//
//     static add(a, b) {
//         return a+b;
//     }
// }
//
// let a = Calculator.add(5,7);
// console.log(a);

//HOMEWORK CLASSES
// class Instrument {
//     constructor(name, type) {
//         this.name = name;
//         this.type = type;
//     }
// }
// class Guitar extends Instrument {
//     // describe(name, type) {
//     describe() {
//         return `I'm a ${this.name} from the ${this.type} family.`;
//     }
// }
// let fender = new Guitar("Fender", "strings");
// console.log(fender);

//Prototypes
// function Wizard(name, house, pet) {
//     this.name = name;
//     this.house = house;
//     this.pet = pet;
//
//     this.greet = () => `I'm ${this.name} from ${this.house}`
// }
// Wizard.prototype.pet_name;
//
// Wizard.prototype.info = function () { //w lokalnych prototypach nie mozna uzywac => funkcji
//     return `I have a ${this.pet} named ${this.pet_name}`
// };
//
// let harry = new Wizard("Harry Potter", "Gryffindor", "Owl");
// harry.pet_name = "Hedwig";
// // console.log(harry);
// // console.log(harry.greet());
// console.log(harry.info());

//HOMEWORK PROTOTYPE
// TODO
// * add a 'color' field to the prototype
// * add a 'bio' method that *returns*:
//      A ${this.color} ${this.make} made in ${this.year}.
//
// function Vehicle(make, year) {
//     this.make = make;
//     this.year = year;
// }
//
// Vehicle.prototype.color;
//
// Vehicle.prototype.bio = function () {
//     return `A ${this.color} ${this.make} made in ${this.year}.`;
// }
//
// let s = new Vehicle("Tesla", 2017);
// s.color = "black";

// let a = new Set();
// a.add(5);
// a.add(43);
// a.add("Wooho!");
// a.add({x: 50, y: 50});
// console.log(a);
// console.log(a.size);
// console.log(a.has(5));
//
// let numbers = [5, 7, 13, 17];
// let numSet = new Set(numbers);
// console.log(numSet);
//
// for (let element of numSet.values()) {
//     console.log(element);
// }
//
// let chars = 'dsxccccdccccfewefew';
// let chars_arr = chars.split("");
// let chars_set = new Set(chars_arr);
// console.log(chars_set);

//HOMEWORK SET
// const contains = (word, letter) => {
//     let letters = word.split("");
//     // TODO create a set with the above 'letters' array
//     let letters_set = new Set(letters);
//     // TODO return whether the set has the 'letter'
//     return letters_set.has(letter);
// };
// let true_check = contains("west", "e");
// let false_check = contains("north", "e");

//Maps
// let a = new Map();
// let key_1 = "string key";
// let key_2 = {a: 'key'};
// let key_3 = function () {};
//
// a.set(key_1, 'return value for a string key');
// a.set(key_2, 'return value for an object key');
// a.set(key_3, 'return value for a function key');
// console.log(a);
//
// let numArr = [[1, "one"], [2, "two"], [3, "three"]];
// let valMap = new Map(numArr);
// // console.log(valMap);
// for (let [key, value] of valMap.entries()) { //entries()-metoda pomocnicza do mapowania iteracyjnego
//     console.log(`${key} points to ${value}`);
// }
//
// let string = 'ujbwdiubwuewoubfiowe';
//
// let letters = new Map();
// for(let i=0; i<string.length; i++) {
//     let letter = string[i];
//     if (!letters.has(letter)) {
//         letters.set(letter, 1);
//     } else {
//         letters.set(letter, letters.get(letter) + 1);
//     }
// }
// console.log(letters);

//HOMEWORK MAPS
// let string = 'supercalifragilisticexpialidocious';
//
// const countLetter = (word, orig_letter) => {
//     // TODO Create a map called 'letters'
//     let letters = new Map();
//     for (let i=0; i<word.length; i++) {
//         let letter = word[i];
//         if (!letters.has(letter)) {
//             // TODO set the letter in the map to 1
//             letters.set(letter, 1);
//         } else {
//             // TODO update the value of letter in letters to its value + 1
//             letters.set(letter, letters.get(letter) + 1);
//         }
//     }
//     return letters.get(orig_letter);
// };
//
// let a_count = countLetter(string, 'a');
// let f_count = countLetter(string, 'f');
// console.log(f_count);

//CLOSURES AND SCOPING
// let call = () => {
//     let secret = 'ES6 rocks!';
//     let reveal = () => {
//         console.log(secret);
//     }
//     // reveal();
//     return reveal;
// }
// // call();
// let unveil = call();
// unveil();

//FUNCTION FACTORIES
// const addSuffix = (x) => {
//     const concat = (y) => {
//         return y + x;
//     }
//     return concat;
// }
// // let add_ness = addSuffix("ness");
// // console.log(add_ness);
// // let h = add_ness("happi");
// // console.log(h);
// let add_ful = addSuffix("ful");
// let f = add_ful("fruit");
// console.log(f);

// const product = (x) => {
//     return y => {
//         return y * x;
//     }
// }
// const product = x => y => y * x;    //prostszy zapis
//
// let mult5 = product(5);
// console.log(mult5(3));
// let double = product(2);
// console.log(double(9));

//PRIVATE METHODS
// const budget = () => {
//     let balance = 0;
//     let changeBal = (val) => {
//         return balance += val;
//     }
//     const deposit20 = () => changeBal(20);
//     const withdraw20 = () => changeBal(-20);
//     const check = () => balance;
//
//     // return {
//     //     deposit20: deposit20,
//     //     withdraw20: withdraw20,
//     //     check: check
//     // }
//     return { deposit20, withdraw20, check }
// }
// let wallet = budget();
// // console.log(wallet);
// wallet.deposit20();
// wallet.withdraw20();
// wallet.deposit20();
// wallet.deposit20();
// console.log(wallet.check());

//GENERATORS
// function* letterMaker() {
//     yield 'a';
//     yield 'b';
//     yield 'c';
// }
//
// let letterGen = letterMaker();
// console.log(letterGen.next().value);
// console.log(letterGen.next().value);
// console.log(letterGen.next().value);
// console.log(letterGen.next().value);
//
// function* countMaker() {
//     let count = 0;
//     while (count < 4) {
//         yield count += 1;
//     }
// }
// let countGen = countMaker();
// console.log(countGen.next().value);
// console.log(countGen.next().value);
// console.log(countGen.next().value);
// console.log(countGen.next().value);
// console.log(countGen.next().value);
//
// function* evens() {
//     let count = 0;
//     while (true) {
//         count += 2;
//         // yield count;
//         let reset = yield count;
//         // if (reset == true) {
//         if(reset) {
//             count = 0;
//         }
//     }
// }
//
// let sequence = evens();
// console.log(sequence.next().value);
// console.log(sequence.next().value);
// console.log(sequence.next().value);
// console.log(sequence.next(true).value);
// console.log(sequence.next().value);

//ITERATOR VS GENERATORS
// const arrayIterator = (array) => {
//     let index = 0;
//
//     return {
//         next: () => {
//             if (index<array.length) {
//                 let next = array[index];
//                 index +=1;
//                 return next;
//             }
//         }
//     }
// }
// let it = arrayIterator(([1,2,3]));
// console.log(it.next());
// console.log(it.next());
// console.log(it.next());
// console.log(it.next());
//
// function* arrayIterator() {
//     // // yield arguments;
//     // for (let arg of arguments) {
//     //     yield arg;
//     // }
//     yield* arguments;
// }
//
// var it = arrayIterator(1, 2, 3);
// console.log(it.next().value);
// console.log(it.next().value);
// console.log(it.next().value);

//PROMISES
// let p = new Promise((resolve, reject) => {
//     // resolve('Resolved promise data');
//     // reject("Rejected promise data");
//     setTimeout(() => resolve('Resolved promise data'), 2000);
// })
//
// p.then(response => console.log(response))
//  .catch(error => console.log(error));
// console.log('after promise consumption');

//APIs
// const root = 'https://www.googleapis.com/books/v1/volumes?q=isbn:0747532699';
//
// fetch(root, {method: "GET"})
//     // .then(response => console.log(response));
//     .then(response => response.json())
//     .then(json => console.log(json));

//ES7
// // let a = Math.pow(2,5);
// let a = 2**5;
// console.log(a);
//
// let b = "wonderful".includes("wonder");
// console.log(b);
//
// let b = [2,3,4,5,6].includes(4);
// console.log(b);

//ES8
// let obj = { a: 'one', b: 'two', c: 'three'};
// // let keys = Object.keys(obj);
// // console.log(keys);   //1,2,3
// // let values = Object.values(obj);
// // console.log(values); // "one", "two", "three"
// let entries = Object.entries(obj);
// // console.log(entries);
// for ( let entry of entries ) {
//     console.log(`key ${entry[0]}, value: ${entry[1]}`);
// }
//ASYNC ES8
// async function async_one() {
//     return "one";
// }
// async function async_two() {
//     // throw new Error('Issue with async!');
//     return "two";
// }
// async_one().then(response =>console.log(response));
// async_two().catch(error => console.log(error));
//
// async function async_three() {
//     const one = await async_one();  //await-funkcja asynchroniczna czeka na poprawną wartość przed kontynuowaniem wykonywania kodu
//     console.log(one);
//     const two = await async_two();
//     console.log(two);
// }
// // async_three();
//
// async function async_four() {
//     const [res_one, res_two] = await Promise.all(
//         [async_one(), async_two()]
//     )
//     console.log(res_one, res_two);
// }
// async_four();

//REACT project

import React from 'react';
import ReactDOM from 'react-dom';
import Global from './components/Global';

ReactDOM.render(
    /*<div>React Application!</div>, document.getElementById('root')*/
    <Global/>, document.getElementById('root')
);
















